using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GetPercent : MonoBehaviour
{

    private TMP_Text _percentA;
    private TMP_Text _percentP;
    private TMP_Text _percentD;
    private TMP_Text _percentT;

    // Start is called before the first frame update
    void Start()
    {
        
        _percentA = GameObject.Find("AcromaticPercent").GetComponent<TMP_Text>();
        _percentA.text = PlayerPrefs.GetString("percentA");
        _percentP = GameObject.Find("ProtanopiaPercent").GetComponent<TMP_Text>();
        _percentP.text = PlayerPrefs.GetString("percentP");
        _percentD = GameObject.Find("DeuteranopiaPercent").GetComponent<TMP_Text>();
        _percentD.text = PlayerPrefs.GetString("percentD");
        _percentT = GameObject.Find("TritanopiaPercent").GetComponent<TMP_Text>();
        _percentT.text = PlayerPrefs.GetString("percentT");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
