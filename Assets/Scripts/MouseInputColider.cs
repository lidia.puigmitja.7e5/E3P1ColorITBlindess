using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
public class MouseInputColider : MonoBehaviour
{
    private GameObject _porcentaje;
    private GameMatrix3 _gameMatrix3;
    private ChangeObjectColor _changeObjectColor;
    private GameObject _fireworks;
    // Start is called before the first frame update
    void Start()
    {
        _porcentaje = GameObject.Find("Resultado");
        _gameMatrix3 = GameObject.Find("GridManager").GetComponent<GameMatrix3>();
        _changeObjectColor = GameObject.Find("GridManager").GetComponent<ChangeObjectColor>();
        _fireworks = GameObject.Find("Firework");
        _fireworks.SetActive(false);
    }

    private IEnumerator TemporarilyActivate(float duration)
    {
        _fireworks.SetActive(true);
        yield return new WaitForSeconds(duration);
        _fireworks.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit raycastHit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out raycastHit))
                if (raycastHit.transform != null)
                {
                    Debug.Log("I got click");
                    if (_porcentaje.GetComponent<TMPro.TMP_Text>().text != "100%" || GameObject.Find("DontSee").GetComponent<GameOver>().FailCount < 2)//O con dos errores!!!!!
                        switch (raycastHit.transform.gameObject.layer)
                    {
                        case 6:
                            //Method for rightguessed color
                            Debug.Log("I'm the correct color");
                                StartCoroutine(TemporarilyActivate(0.6f));
                                SoundManagerScript.PlaySound("CorrectClick");
                                _porcentaje.GetComponent<SumarPercent>().Sumar();
                            _gameMatrix3.DestroyGrid();
                            _changeObjectColor.ChangeIntensity();
                            _gameMatrix3.GenerateGrid();
                            break;
                        default:
                            //Method for wrongguessed color
                            Debug.Log("I'm the incorrect color");
                            GameObject.Find("DontSee").GetComponent<GameOver>().CountDontSee();
                            break;
                    }

                }
        }
    }
}
