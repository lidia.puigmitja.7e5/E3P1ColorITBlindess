using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip GameOverSound, WinSound, CorrectClick, WrongClick;
    static AudioSource audioSrc;


    // Start is called before the first frame update
    void Start()
    {
        GameOverSound = Resources.Load<AudioClip>("GameOverSound");
        WinSound = Resources.Load<AudioClip>("WinSound");
        CorrectClick = Resources.Load<AudioClip>("CorrectClick");
        WrongClick = Resources.Load<AudioClip>("WrongClick");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "GameOverSound":
                audioSrc.PlayOneShot(GameOverSound);
                break;
            case "WinSound":
                audioSrc.PlayOneShot(WinSound);
                break;
            case "CorrectClick":
                audioSrc.PlayOneShot(CorrectClick);
                break;
            case "WrongClick":
                audioSrc.PlayOneShot(WrongClick);
                break;
        }
    }
}
