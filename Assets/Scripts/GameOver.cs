using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
   
    private GameObject _porcentaje;
    private TMP_Text _resultado;
    private GameMatrix3 _gameMatrix3;
    private ChangeObjectColor _changeObjectColor;
    private GestionLevels _gestionLevels;
    private GameObject _death;
    private ShakeBehaviour _shake;
    private CalcularResultat _calcul;
    private GameWin _gameWin;
    private GameObject _click;
    private Button _dontSeeButton;
    private int _failCount = 0;
    public int FailCount
    {
        get => _failCount;
        set => _failCount = value;
    }

    private void Start()
    {
        _death = GameObject.Find("death");
        _death.SetActive(false);
        _porcentaje = GameObject.Find("Resultado");
        _resultado = _porcentaje.GetComponent<TMP_Text>();
        _gameMatrix3 = GameObject.Find("GridManager").GetComponent<GameMatrix3>();
        _changeObjectColor = GameObject.Find("GridManager").GetComponent<ChangeObjectColor>();
        _gestionLevels = GameObject.Find("GridManager").GetComponent<GestionLevels>();
        _shake = GameObject.Find("Main Camera").GetComponent<ShakeBehaviour>();
        _calcul = GameObject.Find("Resultado").GetComponent<CalcularResultat>();
        _click = GameObject.Find("Click");
        _gameWin = GameObject.Find("Resultado").GetComponent<GameWin>();
        _dontSeeButton = GameObject.Find("DontSee").GetComponent<Button>();
    }
  
    public void CountDontSee()
    {
        FailCount++;
        if (FailCount >= 2)
        {
            LoadGameOverScreen();
        }
        else
        {
          
            if (_resultado.text != "0%")
            {
             SoundManagerScript.PlaySound("WrongClick");
            _shake.StartShake(0.2f, 0.5f);
            _porcentaje.GetComponent<SumarPercent>().Restar();
            _gameMatrix3.DestroyGrid();
            _changeObjectColor.RollBackIntensity();
            _gameMatrix3.GenerateGrid();
            }
            else
            {
                //Creo q solucione el problema q no cargaba bien si era primera pantalla d nivel
                SoundManagerScript.PlaySound("WrongClick");
                _shake.StartShake(0.2f, 0.5f);
                _gameMatrix3.DestroyGrid();
                _changeObjectColor.RollBackIntensity();
                _gameMatrix3.GenerateGrid();

            }
           
        }
            
    }

    private IEnumerator TemporarilyActivate(float duration)
    {
        _resultado.text = _calcul.CalcularResultats(_resultado.text);
        _gameMatrix3.DestroyGrid();
        _dontSeeButton.interactable = false;
        _click.SetActive(false);   
        SoundManagerScript.PlaySound("GameOverSound");
        _death.SetActive(true);
        yield return new WaitForSeconds(duration);
        _death.SetActive(false);
        _gestionLevels.NewLevel();
        _gameMatrix3.GenerateGrid();
        _click.SetActive(true);
        _dontSeeButton.interactable = true;
    }

    private IEnumerator TemporarilyActivateEnd(float duration)
    {
        if (_resultado.text == "95%" || _resultado.text == "90%")
        {
            _death.SetActive(false);
            _click.SetActive(false);
            _gameWin.LoadWinScreen();
        }
        else
        {
            _resultado.text = _calcul.CalcularResultats(_resultado.text);
            _gameMatrix3.DestroyGrid();
            _click.SetActive(false);
            SoundManagerScript.PlaySound("GameOverSound");
            _death.SetActive(true);
            _dontSeeButton.interactable = false;
        }
        yield return new WaitForSeconds(duration);
        SceneManager.LoadScene("FinalGame", LoadSceneMode.Single);

    }

    public void LoadGameOverScreen()
    {
        FailCount = 0;
        _changeObjectColor.TouchCount = 0;
        if (_gestionLevels.Level == Nivel.Nivel4)
        {
            StartCoroutine(TemporarilyActivateEnd(6f));

        }
        else
        {
            if (_resultado.text == "95%" || _resultado.text == "90%")
            {
                _death.SetActive(false);
                _gameWin.LoadWinScreen();
            }
            else
            {
                StartCoroutine(TemporarilyActivate(6f));
            }
           
        }

    }
}
