using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameWin : MonoBehaviour
{
    private GestionLevels _gestionLevels;
    private GameObject _winCup;
    private GameMatrix3 _gridManager;
    private GameOver _dontsee;
    private TMP_Text _resultado;
    private CalcularResultat _calcularResultat;
    private ChangeObjectColor _changeObjectColor;
    private Button _dontSeeButton;
    private void Start()
    {
        _winCup = GameObject.Find("winCup");
        _winCup.SetActive(false);
        _gestionLevels = GameObject.Find("GridManager").GetComponent<GestionLevels>();
        _gridManager = GameObject.Find("GridManager").GetComponent<GameMatrix3>();
        _dontsee = GameObject.Find("DontSee").GetComponent<GameOver>();
        _dontSeeButton = GameObject.Find("DontSee").GetComponent<Button>();
        _resultado = GameObject.Find("Resultado").GetComponent<TMP_Text>();
        _calcularResultat = GameObject.Find("Resultado").GetComponent<CalcularResultat>();
        _changeObjectColor = GameObject.Find("GridManager").GetComponent<ChangeObjectColor>();
    }

    private void Update()
    {
        if (_resultado.text == "100%")
            LoadWinScreen();
    }


    private IEnumerator TemporarilyActivate(float duration)
    {
        _gridManager.DestroyGrid();
        _winCup.SetActive(true);
        _dontSeeButton.interactable = false;
        SoundManagerScript.PlaySound("WinSound");
        yield return new WaitForSeconds(duration);
        _winCup.SetActive(false);
        _gestionLevels.NewLevel();
        _dontSeeButton.interactable = true;
        _gridManager.GenerateGrid();

    }

    private IEnumerator TemporarilyActivateEnd(float duration)
    {
        _dontSeeButton.interactable = false;
        _gridManager.DestroyGrid();
        _winCup.SetActive(true);
        SoundManagerScript.PlaySound("WinSound");
        yield return new WaitForSeconds(duration);
        SceneManager.LoadScene("FinalGame", LoadSceneMode.Single);

    }

    public void LoadWinScreen()
    {
        _changeObjectColor.TouchCount = 0;
        _resultado.text = _calcularResultat.CalcularResultats(_resultado.text);
        _dontsee.FailCount = 0;
        if (_gestionLevels.Level == Nivel.Nivel4) 
        {
            StartCoroutine(TemporarilyActivateEnd(5f));
        }
        else
        {
            StartCoroutine(TemporarilyActivate(5f));
        }
       
    }
}
