using TMPro;
using UnityEngine;


public enum Nivel
{
    Nivel1,
    Nivel2,
    Nivel3,
    Nivel4,
    Fin
}
public class GestionLevels : MonoBehaviour
{
    private Nivel _nivel;
    private int _count;
    public int Count
    {
        get => _count;
        set=>_count = value;
    }
    private TMP_Text _tituloNivel;
    public Nivel Level
    {
        get => _nivel;
        set => _nivel = value;
    }
    // Start is called before the first frame update
    void Start()
    {
        Count = 0;
        _tituloNivel = GameObject.Find("TituloNivel").GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (Count)
        {
            case 0:
                _nivel = Nivel.Nivel1;
                _tituloNivel.text = "Nivel 1";
                break;
            case 1:
                _nivel = Nivel.Nivel2;
                _tituloNivel.text = "Nivel 2";
                break;
            case 2:
                _nivel = Nivel.Nivel3;
                _tituloNivel.text = "Nivel 3";
                break;
            case 3:
                _nivel = Nivel.Nivel4;
                _tituloNivel.text = "Nivel 4";
                break;
     
            default:
                Debug.Log("Error: Level no selected");
                break;
        }
    }

    public void NewLevel()
    {
        if(Count < 3)
            Count++;
    }
}
