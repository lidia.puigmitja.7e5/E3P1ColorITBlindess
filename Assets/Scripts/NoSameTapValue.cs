using UnityEngine;

//Se necesita la matriz currentTapped para tener un registro de donde han aparecido ya los cuadrados con el color correcto.
//El m�todo del script puede ser movido al script de la matriz si es necesario. 
public class NoSameTapValue : MonoBehaviour
{
    private GameObject[,] currentTapped;
    
    public bool NoSamePosition(GameObject[,] matrix, GameObject[,] positionValues)
    {
        for(var y = 0; y < matrix.GetLength(0); y++)
        {
            for(var x = 0; x < matrix.GetLength(1); x++)
            {
                if (matrix[y, x].tag == "correct")
                {
                    if (positionValues[y, x] != null)
                        return true;
                    else
                    {
                        positionValues[y, x] = matrix[y, x];
                        return false;
                    }
                }
            }
        }
        return false;
        
    }
}
