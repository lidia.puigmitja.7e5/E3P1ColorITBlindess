using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace UI {
    //Clase que coge el GameObject con el nombre del jugador y lo muestra en el GameObject Name. 
    public class PlayerName : MonoBehaviour
    {


        // Start is called before the first frame update
        //El Nombre del jugador adquiere el nombre guardado en el objeto persitente PlayerName. Para conservar el nombre de usuarios entre escenas.
        void Start()
        {
           
            this.gameObject.GetComponent<TMP_Text>().text = GameObject.Find("PlayerName").GetComponent<TMP_Text>().text;
        
        }

    }
}
