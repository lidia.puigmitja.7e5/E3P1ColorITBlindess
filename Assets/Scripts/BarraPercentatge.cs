using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BarraPercentatge : MonoBehaviour
{
    private Image progressAcro;
    private Image progressPro;
    private Image progressDeu;
    private Image progressTri;

    private TMP_Text Acromatic;
    private TMP_Text Protanopia;
    private TMP_Text Deuteranopia;
    private TMP_Text Tritanopia;


    // Start is called before the first frame update
    void Start()
    {
        progressAcro = GameObject.Find("RellenoA").GetComponent<Image>();
        progressPro = GameObject.Find("RellenoP").GetComponent<Image>();
        progressDeu = GameObject.Find("RellenoD").GetComponent<Image>();
        progressTri = GameObject.Find("RellenoT").GetComponent<Image>();

        Acromatic = GameObject.Find("AcromaticPercent").GetComponent<TMP_Text>();
        Protanopia = GameObject.Find("ProtanopiaPercent").GetComponent<TMP_Text>();
        Deuteranopia = GameObject.Find("DeuteranopiaPercent").GetComponent<TMP_Text>();
        Tritanopia = GameObject.Find("TritanopiaPercent").GetComponent<TMP_Text>();

    
    }

    // Update is called once per frame
    void Update()
    {

        progressAcro.fillAmount = (float)(ConvertirPercent(Acromatic.text) / 100);
        Debug.Log("Acro : " + (float)(ConvertirPercent(Acromatic.text) / 100));

        progressPro.fillAmount = (float)(ConvertirPercent(Protanopia.text) / 100);
        Debug.Log("Pro : " + (float)(ConvertirPercent(Protanopia.text) / 100));

        progressDeu.fillAmount = (float)(ConvertirPercent(Deuteranopia.text) / 100);
        Debug.Log("Deu : " + (float)(ConvertirPercent(Deuteranopia.text) / 100));

        progressTri.fillAmount = (float)(ConvertirPercent(Tritanopia.text) / 100);
        Debug.Log("Tri : " + (float)(ConvertirPercent(Tritanopia.text) / 100));

    }

    float ConvertirPercent(string text)
    {
        int resultat;
        text = text.Remove(text.Length - 1);
        resultat = Convert.ToInt32(text);
        return resultat;
    }
}
