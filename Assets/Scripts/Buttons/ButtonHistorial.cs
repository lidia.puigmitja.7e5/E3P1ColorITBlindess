using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using TestIshiharaCode;

public class ButtonHistorial : ButtonChangeScene
{
    private GameObject _popUp;
    //El botton no va al historial pq n se puede x el path youknow
    protected override void Start()
    {
        base.Start();
       _popUp = GameObject.Find("PopUpHistorial");
        _popUp.SetActive(false);
    }

    // Start is called before the first frame update
    protected override void TaskOnClick()
    {/*
      string path = Application.dataPath + "/StreamingAssets/testResults.json";
        bool filexists1 = ResultSave.IfFileExists(path);
        if (filexists1 == false)
            ResultSave.CreateJsonFile(path);
        base.TaskOnClick();
        */
        _popUp.SetActive(true);
        //Funcion que se encarga de hacer esperar 1.5 segundos antes de desaparecer el pop.up. 
        StartCoroutine(CloseAfterTime(1.5f));

    }
    public IEnumerator CloseAfterTime(float t)
    {
        yield return new WaitForSeconds(t);
        _popUp.SetActive(false);
    }
}
