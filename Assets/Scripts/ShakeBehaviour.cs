using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeBehaviour : MonoBehaviour
{
    [SerializeField] private float tiempoPrueba; //0.2
    [SerializeField] private float fuerzaPrueba;//0.5
    [SerializeField] private float cantidadRotacion=0.3f;//0.3
    [SerializeField] private float cantidadFuerza=1; //1

    private float tiempoRestante, fuerzaShake, tiempo, rotacion;
 

    private Vector3 posIni;
    private bool shake;

    public ShakeBehaviour instance;


    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        shake = false;
    }

    private void LateUpdate()
    {
        {
            if (shake)
            {
                if (tiempoRestante > 0f)
                {
                    tiempoRestante -= Time.deltaTime;
                    float cantidadX = posIni.x + Random.Range(-cantidadFuerza, cantidadFuerza) * fuerzaShake;
                    float cantidadY = posIni.y + Random.Range(-cantidadFuerza, cantidadFuerza) * fuerzaShake;
                    cantidadX = Mathf.MoveTowards(cantidadX, posIni.x, tiempo * Time.deltaTime);
                    cantidadY = Mathf.MoveTowards(cantidadY, posIni.y, tiempo * Time.deltaTime);
                    transform.position = new Vector3(cantidadX, cantidadY, posIni.z);

                    rotacion = Mathf.MoveTowards(rotacion, 0f, tiempo * cantidadRotacion * Time.deltaTime);
                    transform.rotation = Quaternion.Euler(0f, 0f, rotacion * Random.Range(-1f, 1f));
                }
                else
                {
                    transform.position = posIni;
                    shake = false;
                }

            }
        }
    }

    public void StartShake(float duracion, float fuerza)
    {
        posIni = transform.position;
        shake = true;
        tiempoRestante = duracion;
        fuerzaShake = fuerza;
        tiempo = fuerza / duracion;
        rotacion = fuerza * cantidadRotacion;
    }

    // Update is called once per frame
    void Update()
    {
     

    }

   
}
