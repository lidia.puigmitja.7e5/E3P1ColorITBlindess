using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResetScript : MonoBehaviour
{
    private ChangeObjectColor _changeObjectColor;
    private GestionLevels _gestionLevels;
    private TMP_Text _porcentaje;
    private GameOver _dontsee;
    private Button _dontSeeButton;
    // Start is called before the first frame update
    void Start()
    {
        _dontSeeButton = GameObject.Find("DontSee").GetComponent<Button>();
        _changeObjectColor.TouchCount = 0;
        _dontSeeButton.interactable = true;
        _dontsee = GetComponent<GameOver>();
        _dontsee.FailCount = 0;
        _gestionLevels= GetComponent<GestionLevels>();
        _gestionLevels.Count = 0;
        _porcentaje = GameObject.Find("PorcentajeA").GetComponent<TMP_Text>();
        _porcentaje.text = "0%";
        _porcentaje = GameObject.Find("PorcentajeP").GetComponent<TMP_Text>();
        _porcentaje.text = "0%";
        _porcentaje = GameObject.Find("PorcentajeD").GetComponent<TMP_Text>();
        _porcentaje.text = "0%";
        _porcentaje = GameObject.Find("PorcentajeT").GetComponent<TMP_Text>();
        _porcentaje.text = "0%";
        _porcentaje = GameObject.Find("Resultado").GetComponent<TMP_Text>();
        _porcentaje.text = "0%";

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
