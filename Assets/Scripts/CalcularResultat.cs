using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CalcularResultat : MonoBehaviour
{
    private Nivel niveles;

    // Start is called before the first frame update
    void Start()
    {  
        niveles = GameObject.Find("GridManager").GetComponent<GestionLevels>().Level;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public string CalcularResultats(string text)
    {
        int resultat;
        text = text.Remove(text.Length - 1);
        resultat = Convert.ToInt32(text);
        string nivel="";
        string diagnostic = "";
        switch (niveles)
        {
            case Nivel.Nivel1:
                nivel = "acrom�tic";
                break;
            case Nivel.Nivel2:
                nivel = "de tipus protanopia";
                break;
            case Nivel.Nivel3:
                nivel = "de tipus deuteranopia";
                break;
            case Nivel.Nivel4:
                nivel = "de tipus tritanopia";
                break;
       
            default:
                break;

        }
        Debug.Log("Resultat: " + resultat);
        if (resultat >= 90) diagnostic = "Bona visi�";
        else if (resultat < 90 && resultat >= 60) { diagnostic = "Visi� lleument afectada, possible daltonisme " + nivel; Debug.Log("Resultat : " + resultat); }
        else if (resultat < 60 && resultat >= 30) { diagnostic = "Visi� moderadament afectada, possible daltonisme " + nivel; Debug.Log("Resultat : " + resultat); }
        else if (resultat < 30 && resultat >= 0) { diagnostic = "Visi� severament afectada, possible daltonisme " + nivel; Debug.Log("Resultat : " + resultat); }

        return diagnostic;
    }


}

