using TMPro;
using UnityEngine;

public class SumarPercent : MonoBehaviour
{

    private TMP_Text _porcentaje;
    private TMP_Text _resultado;
    private int _resultat;
    private Nivel nivel;
    private ChangeObjectColor _changeObjectColor;
    // Start is called before the first frame update
    void Start()
    {
        _changeObjectColor = GameObject.Find("GridManager").GetComponent<ChangeObjectColor>();
        _resultat = 0;
        _resultado = gameObject.GetComponent<TMP_Text>();
    }

    // Update is called once per frame
    void Update()
   {
        nivel = GameObject.Find("GridManager").GetComponent<GestionLevels>().Level;
        switch (nivel)
        {
            case Nivel.Nivel1:
                _porcentaje = GameObject.Find("PorcentajeA").GetComponent<TMP_Text>();
                break;
            case Nivel.Nivel2:
                _porcentaje = GameObject.Find("PorcentajeP").GetComponent<TMP_Text>();
                break;
            case Nivel.Nivel3:
                _porcentaje = GameObject.Find("PorcentajeD").GetComponent<TMP_Text>();
                break;
            case Nivel.Nivel4:
                _porcentaje = GameObject.Find("PorcentajeT").GetComponent<TMP_Text>();
                break;
            default:
                break;

        }
        if (_changeObjectColor.TouchCount == 0)
            _resultat = 0;
    }

    public void Sumar()
    {
        _resultat += 5;
        _porcentaje.text = _resultat.ToString()+"%";
        _resultado.text = _resultat.ToString() + "%";

    }

    public void Restar()
    {
        if(_resultat > 0)
        {
            _resultat -= 5;
        }
        _porcentaje.text = _resultat.ToString() + "%";
        _resultado.text = _resultat.ToString() + "%";
    }

    public string GetPercent()
    {
        return _porcentaje.text;
    }
}
