using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SavePercent : MonoBehaviour
{

    private GameObject _percentA;
    private GameObject _percentP;
    private GameObject _percentD;
    private GameObject _percentT;

    // Start is called before the first frame update
    void Start()
    {

        _percentA = GameObject.Find("PorcentajeA");
       
        _percentP = GameObject.Find("PorcentajeP");
        
        _percentD = GameObject.Find("PorcentajeD");
        
        _percentT = GameObject.Find("PorcentajeT");
        
    }

    // Update is called once per frame
    void Update()
    {

        PlayerPrefs.SetString("percentA", _percentA.GetComponent<TMP_Text>().text);
        PlayerPrefs.SetString("percentP", _percentP.GetComponent<TMP_Text>().text);
        PlayerPrefs.SetString("percentD", _percentD.GetComponent<TMP_Text>().text);
        PlayerPrefs.SetString("percentT", _percentT.GetComponent<TMP_Text>().text);
    
    }
}
