using UnityEngine;



public class ChangeObjectColor : MonoBehaviour
{
    private float[] _touchColor;
    private float[] _backgroundColor;
    private float[] _objectiveColor;
    private SpriteRenderer _sprite;
    private GameObject[] _gameElements;
    private float _frameRate;
    private float _timer;
    private Nivel nivel;
    private int touchCount = 0;
    public int TouchCount
    {
        get => touchCount;
        set => touchCount = value;
    }
    private float[] _colorTone;
    private float _limitColor;
    // Start is called before the first frame update
    void Start()
    {
        _frameRate = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (touchCount == 0)
        {
           nivel = GameObject.Find("GridManager").GetComponent<GestionLevels>().Level;
            ColorForLevel();
        }
        _gameElements = GameObject.FindGameObjectsWithTag("gameElement");
        if (_timer >= 1 / _frameRate)
        {
            foreach (var gameObject in _gameElements)
            {
                _sprite = gameObject.GetComponent<SpriteRenderer>();
                if (gameObject.layer == 6)
                {
                    switch (nivel)
                    {
                        case Nivel.Nivel1:
                            ChangeColorForSprite(GenerateColor(_touchColor[0], _touchColor[1]), _sprite);
                            break;
                        case Nivel.Nivel4:
                            if (touchCount >= 10)
                                ChangeColorForSprite(GeneratePurpleColor(_touchColor[0], _touchColor[1]), _sprite);
                            else
                                ChangeColorForSprite(GenerateColor(_touchColor[0], _touchColor[1], _touchColor[2], _touchColor[3], _touchColor[4], _touchColor[5]), _sprite);
                            break;
                        default:
                            ChangeColorForSprite(GenerateColor(_touchColor[0], _touchColor[1], _touchColor[2], _touchColor[3], _touchColor[4], _touchColor[5]), _sprite);
                            break;
                    }
                    
                }
                else
                    switch(nivel){
                    case Nivel.Nivel1:
                            ChangeColorForSprite(GenerateColor(_backgroundColor[0], _backgroundColor[1]), _sprite);
                    break;
                        case Nivel.Nivel4:
                        ChangeColorForSprite(GeneratePurpleColor(_backgroundColor[0], _backgroundColor[1]), _sprite);
                    break;
                    default:
                            ChangeColorForSprite(GenerateColor(_backgroundColor[0], _backgroundColor[1], _backgroundColor[2], _backgroundColor[3], _backgroundColor[4], _backgroundColor[5]), _sprite);
                    break;
                }

            }
            _timer = 0;
        }
        _timer += Time.deltaTime;
    }

    void ColorForLevel()
    {
        switch (nivel)
        {
            case Nivel.Nivel1:
                NNumberOfParamsInArray(ref _backgroundColor, new float[] { 19, 73 });
                NNumberOfParamsInArray(ref _touchColor, new float[] { 137, 178 });
                ObtainTheColorTone(20, ref _colorTone, _backgroundColor, _touchColor,1);
                _limitColor = -2;
                break;
            case Nivel.Nivel2:
                NNumberOfParamsInArray(ref _backgroundColor, new float[] { 9, 35, 35, 140, 4, 5 });
                NNumberOfParamsInArray(ref _objectiveColor, new float[] {170, 255, 35, 140, 5, 20});
                NNumberOfParamsInArray(ref _touchColor, new float[] { 87, 172, 0, 0, 0, 0 });
                _limitColor = 10;
                ObtainTheColorTone(_limitColor, ref _colorTone, _objectiveColor, _touchColor,2);
                break;
            case Nivel.Nivel3:
                NNumberOfParamsInArray(ref _backgroundColor, new float[] { 112, 204, 8, 14, 72, 131 });
                NNumberOfParamsInArray(ref _objectiveColor, new float[] {83, 111, 83, 111, 83,104});
                NNumberOfParamsInArray(ref _touchColor, new float[] { 30, 51, 57, 111, 83, 104 });
                _limitColor = 10;
                ObtainTheColorTone(_limitColor, ref _colorTone, _objectiveColor, _touchColor,2);
                break;
            case Nivel.Nivel4:
                NNumberOfParamsInArray(ref _backgroundColor, new float[] { 27, 70, 0, 0, 27, 70 });
                NNumberOfParamsInArray(ref _objectiveColor, new float[] { 56, 150, 0, 0, 56, 150 });
                NNumberOfParamsInArray(ref _touchColor, new float[]  { 0, 0, 0, 0, 56, 125 });
                _limitColor = 10;
                ObtainTheColorTone(_limitColor, ref _colorTone, _objectiveColor, _touchColor,2);
                break;
        }
    }

    void NNumberOfParamsInArray(ref float[] array, params float[] numbers)
    {
        array = new float[numbers.Length];
        for (int i = 0; i < numbers.Length; i++)
        {
            array[i] = numbers[i];
        }
    }

    void ObtainTheColorTone(float numberOfTimes, ref float[] colorTone,float[] objectiveColor, float[] touchColor, float separateIntenisty)
    {
        colorTone = new float[objectiveColor.Length];
            for (int i = 0; i < objectiveColor.Length; i++)
                        colorTone[i] = (objectiveColor[i] - touchColor[i]) / numberOfTimes+separateIntenisty;
    }

    Color GenerateColor(float rangeColorRedMin, float rangeColorRedMax, float rangeColorGreenMin, float rangeColorGreenMax, float rangeColorBlueMin, float rangeColorBlueMax)
    {
        double red = RGBAtransform(RandomValue(rangeColorRedMin, rangeColorRedMax));
        double blue = RGBAtransform(RandomValue(rangeColorGreenMin, rangeColorGreenMax));
        double green = RGBAtransform(RandomValue(rangeColorBlueMin, rangeColorBlueMax));
        return new Color((float)red, (float)blue, (float)green);
    }


    Color GenerateColor(float rangeColorGreyMin, float rangeColorGreyMax)
    {
        double grey = RGBAtransform(RandomValue(rangeColorGreyMin, rangeColorGreyMax));
        return new Color((float)grey, (float)grey, (float)grey);
    }

    Color GeneratePurpleColor(float rangeColorPurpleMin, float rangeColorPurpleMax)
    {
        double red = RGBAtransform(RandomValue(rangeColorPurpleMin, rangeColorPurpleMax));
        double blue = 0;
        double green = red;
        return new Color((float)red, (float)blue, (float)green);
    }

    void ChangeColorForSprite(Color newColor, SpriteRenderer sprite)
    {
        sprite.color = newColor;
    }
    int RandomValue(float min, float max)
    {
        var rnd = new System.Random();
        return rnd.Next((int)min, (int)max + 1);
    }

    double RGBAtransform(int value)
    {
        return System.Math.Round((double)value / 255, 2);
    }

    public void ChangeIntensity()
    {
        touchCount++;
        if (touchCount == _limitColor)
            ObtainTheColorTone(20 - _limitColor, ref _colorTone, _backgroundColor, _touchColor,2);
        for (int i = 0; i < _touchColor.Length; i++)
            _touchColor[i] += _colorTone[i];
        
        Debug.Log("I changed intensity");
        Debug.Log(touchCount);
        if (touchCount == 20)
            touchCount = 0;        
    }

    public void RollBackIntensity()
    {
        if (touchCount <= 0)
        {
             touchCount = 0;
        }   
        else
        {
        if (touchCount == _limitColor)
            ObtainTheColorTone(20 - _limitColor, ref _colorTone, _objectiveColor, _touchColor,2);
        touchCount--;
        for (int i = 0; i < _touchColor.Length; i++)
            _touchColor[i] -= _colorTone[i];
        Debug.Log("I changed intensity");
        Debug.Log(touchCount);
        }
    }
}
