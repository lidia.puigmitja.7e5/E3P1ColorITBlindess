using UnityEngine;

public class GameMatrix3 : MonoBehaviour
{
    [SerializeField] private int _width, _height;

    [SerializeField] private GameObject _tilePrefab;

    [SerializeField] private Transform _cam;
    private int[] _positionsX;
    private int[] _positionsY;
    private int xRight;
    private int yRight;
    private GameObject[,] _matrix;

    private void Awake()
    {
        _matrix = new GameObject[_width, _height];
        _positionsX = System.Array.Empty<int>();
        _positionsY = System.Array.Empty<int>();
        GenerateGrid();
    }
    void Start()
    {
    }

   public void GenerateGrid()
    {
        do
        {
            var rnd = new System.Random();
            xRight = rnd.Next(1, _width - 2);
            yRight = rnd.Next(1, _height - 2);
        } while (NoSamePosition(xRight, yRight, ref _positionsX, ref _positionsY));
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                var spawnedTile =Instantiate(_tilePrefab, new Vector3(x, y), Quaternion.identity);
                spawnedTile.name = $"Tile {x} {y}";
                spawnedTile.tag = "gameElement";
                if (y <= yRight + 1 && y >= yRight - 1 && x <= xRight + 1 && x >= xRight - 1)
                    spawnedTile.layer = 6;
                else
                    spawnedTile.layer = 0;
               _matrix[x, y] = spawnedTile;
            }
        }        
        
        _cam.transform.position = new Vector3((float)_width / 2 - 0.5f, (float)_height / 2 - 0.5f, -10);
    }


    public void DestroyGrid()
    {
        for(int y = 0; y < _width; y++)
        {
            for(int x = 0; x < _height; x++)
                Destroy(_matrix[x, y]);
        }
    }

    public bool NoSamePosition(int positionX, int positionY, ref int[] positionValuesX, ref int[] positionValuesY)
    {
        for (var i = 0; i < positionValuesX.Length; i++)
        {
            if (positionX == positionValuesX[i] && positionValuesY[i] == positionY)
                return true;
        }
        System.Array.Resize(ref positionValuesX, positionValuesX.Length + 1);
        System.Array.Resize(ref positionValuesY, positionValuesY.Length + 1);
        positionValuesX[^1] = positionX;
        positionValuesY[^1] = positionY;
        return false;

    }
}
